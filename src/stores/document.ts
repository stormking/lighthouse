import { defineStore } from 'pinia'
import { Graph, Node } from 'starqld';
import { v4 as uuid } from 'uuid'
import GraphConfig from '@/assets/graph.json';

export const useMainStore = defineStore('document', {
	state: () => ({
		root: new Graph()
	}),
	getters: {
		list(): Node[] {
			return this.root.getGraph()
		},
		byId() {
			return (id: string): Node => {
				if (!id) {
					console.error('no id given for docStore.byId')
					let n = Node.createEmpty();
					n.setLiteral(GraphConfig.labelProp, 'no-id-given')
					return n;
				}
				return this.root.getNodeById(id);
			}
		},
		byType() {
			return (type: string): Node[] => {
				return this.list.filter(e => e.getType() === type)
			}
		},
		getListOfTypes(): Node[] {
			return this.byType(GraphConfig.typeRoot)
		},
		getUserContent() {
			return (type?: string): Node[] => {
				let types = type ? [type] : this.getListOfTypes.map(e => e.getId());
				return this.list.filter(e => types.includes(e.getType()))
			}
		},
		linkTypes(): Node[] {
			return this.byType(GraphConfig.linkRoot)
		},
		getOutgoingNodes() {
			return (from: Node): { link: Node, target: Node }[] => {
				return this.linkTypes.map(link => {
					let prop = link.getId()
					return { link, targets: from.getAll(prop) }
				}).flatMap(({ link, targets }) => {
					return targets.map(e => ({ link, target: e }))
				})
			}
		},
		getIncomingLinks() {
			return (to: Node): { link: Node, from: Node }[] => {
				const id = to.getId();
				return this.getUserContent().flatMap(n => {
					return this.linkTypes.flatMap(link => {
						let targets = n.getAll(link.getId());
						let match = !!targets.find(n => n.getId() === id);
						if (!match) return [];
						return [{ link, from: n }];
					})
				})
			}
		}
	},
	actions: {
		openDoc(doc: Graph) {
			this.root = doc;
		},
		addBlankNode(id?: string): Node {
			if (!id) id = uuid();
			if (id!.substring(0, 4) !== 'http') id = 'http://a.cryptic.link/owl/id#'+id;
			return this.root.addNewNodeWithId(id!);
		},
		updateNode(node: Node) {
			console.log('update node', node);
			let exists = this.root.getNodeById(node.getId(), true);
			if (exists) {
				exists.replaceData(node);
			} else {
				this.root.addExternalNode(node);
			}
		},
		removeNode(id: string) {
			let exists = this.root.getNodeById(id, true);
			if (!exists) return;
			this.root.remove(exists);
		}
	}
})
