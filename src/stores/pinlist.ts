import { defineStore } from 'pinia'

const MaxCountPerList = 5;
const MaxCountTotal = 15;

export const usePinStore = defineStore('pins', {
	state: () => ({
		pinned: [] as string[],
		lastViewed: [] as string[],
		lastSaved: [] as string[]
	}),
	getters: {
		allPins() {

		}
	},
	actions: {
		setPin(nodeId: string, add: boolean) {
			console.log('setPin', nodeId, add);
			let idx = this.pinned.findIndex(e => e === nodeId);
			if (idx < 0 && add) {
				this.pinned.push(nodeId);
			} else if (idx > -1 && !add) {
				this.pinned.splice(idx, 1);
			}
		},
		resetPins() {
			this.pinned = [];
		},
		addLastViewed(nodeId: string) {
			let idx = this.lastViewed.findIndex(e => e === nodeId);
			if (idx > -1) {
				this.lastViewed.splice(idx, 1);
			}
			this.lastViewed.push(nodeId);
		},
		addLastSaved(nodeId: string) {
			let idx = this.lastSaved.findIndex(e => e === nodeId);
			if (idx > -1) {
				this.lastSaved.splice(idx, 1);
			}
			this.lastSaved.push(nodeId);
			console.log('addLastSaved', nodeId);
		}
	}
})
