import { defineStore } from 'pinia'
import type { Node } from 'starqld'
import { useMainStore } from './document'
import GraphConfig from '@/assets/graph.json';
import { throttle } from 'throttle-debounce';

type Position = { x: number, y: number }
type NodeInfo = {
	id: string,
	x: number,
	y: number,
	width: number,
	height: number
}
type LinkPosition = {
	from: Position,
	to: Position,
	label: string
}
type SetPositionLogical = {
	node: Node,
	column: number,
	position: number
}

const reflow = throttle(250, (nodeInfos: NodeInfo[]) => {
	let heightIndex = new Map<number, number>();
	const margin = 24;
	nodeInfos.forEach(info => {
		let h = margin;
		if (heightIndex.has(info.x)) {
			h = heightIndex.get(info.x)!;
		}
		info.y = h;
		h += info.height + margin;
		heightIndex.set(info.x, h);
	})
}, { noLeading: true, noTrailing: false })

export const useLensStore = defineStore('lens', {
	
	state: () => ({
		containerWidth: 100,
		containerHeight: 100,
		nodeWidth: 360,
		focusNodeId: '',
		nodeInfos: [] as NodeInfo[]
	}),

	getters: {
		positionForNode() {
			return (node: Node): NodeInfo => {
				let entry = this.nodeInfos.find(e => e.id === node.getId());
				if (!entry) {
					entry = {
						id: node.getId(),
						x: 0,
						y: 0,
						width: this.nodeWidth,
						height: 120
					};
				}
				return entry;
			}
		},
		linkPositions(): LinkPosition[] {
			if (!this.focusNodeId) return [];
			let docStore = useMainStore();
			let idx = new Map(this.nodeInfos.map(pos => {
				return [
					pos.id,
					{
						node: docStore.byId(pos.id),
						x: pos.x,
						y: pos.y,
						w: pos.width,
						h: pos.height
					}
				]
			}));
			let focusNode = docStore.byId(this.focusNodeId);
			let incoming = docStore.getIncomingLinks(focusNode);
			let outgoing = docStore.getOutgoingNodes(focusNode);
			let res: LinkPosition[] = [];
			let label = (node) => node.getLiteral(GraphConfig.labelProp)
			incoming.forEach(entry => {
				let pf = idx.get(entry.from.getId())
				let pt = idx.get(this.focusNodeId);
				if (!pf || !pt) return;
				res.push({
					label: label(entry.link),
					from: {
						x: pf.x + pf.w,
						y: pf.y + pf.h/2
					},
					to: {
						x: pt.x + pt.w/2,
						y: pt.y + pt.h/2
					}
				})
			})
			outgoing.forEach(entry => {
				let pf = idx.get(this.focusNodeId)
				let pt = idx.get(entry.target.getId());
				if (!pf || !pt) return;
				res.push({
					label: label(entry.link),
					from: {
						x: pf.x + pf.w/2,
						y: pf.y + pf.h/2
					},
					to: {
						x: pt.x,
						y: pt.y + pt.h/2
					}
				})
			})
			// console.log('update link pos', res);
			return res;
		}
	},

	actions: {
		init(containerWidth: number, containerHeight: number, focusNodeId: string) {
			this.containerHeight = containerHeight;
			this.containerWidth = containerWidth;
			this.focusNodeId = focusNodeId;
		},
		setNodePosition(node: Node, pos: Position) {
			let entry = this.nodeInfos.find(e => e.id === node.getId());
			if (!entry) {
				entry = {
					id: node.getId(),
					x: 0,
					y: 0,
					width: this.nodeWidth,
					height: 120
				};
				this.nodeInfos.push(entry)
			}
			entry.x = pos.x;
			entry.y = pos.y;
		},
		setNodePositionLogical(npos: SetPositionLogical) {
			let pos: Position = {
				x: 0,
				y: 0
			}
			if (npos.column === 0) {
				pos.x = 0;
			}
			if (npos.column !== 1) {
				pos.y = 200 * npos.position;
			} else {
				pos.y = 200;
				pos.x = this.containerWidth/2 - this.nodeWidth/2
			}
			if (npos.column === 2) {
				pos.x = this.containerWidth - this.nodeWidth
			}
			this.setNodePosition(npos.node, pos);
		},
		setSize(node: Node, rect: DOMRect) {
			let entry = this.nodeInfos.find(e => e.id === node.getId());
			if (!entry) {
				entry = {
					id: node.getId(),
					x: 0,
					y: 0,
					width: 0,
					height: 0
				};
				this.nodeInfos.push(entry)
			}
			entry.width = rect.width;
			entry.height = rect.height;
			this.reflowCards();
		},
		reflowCards() {
			reflow(this.nodeInfos)
		}
	}

})
