import { Graph } from 'starqld';
import { useMainStore } from '@/stores/document';

const mixin = {
	'@context': {
		'@base': 'http://a.cryptic.link/owl/',
		'p': 'http://a.cryptic.link/owl/prop/',
		't': 'http://a.cryptic.link/owl/type/',
		'a': 'http://a.cryptic.link/owl/attribute/',
		'rdfs': 'http://www.w3.org/2000/01/rdf-schema#',
		'rdf': 'http://www.w3.org/1999/02/22-rdf-syntax-ns#',
		'xsd': 'http://www.w3.org/2001/XMLSchema#'
	}
}

const Roots = [
	{
		'@id': 'root',
		'@type': 'rdfs:class',
		'rdfs:label': { '@value': 'Root' }
	},
	{
		'@id': 'root/type',
		'@type': 'root',
		'rdfs:label': { '@value': 'Types' },
		'p:attributeset': { '@id': 'attribute/type' }
	},
	{
		'@id': 'root/attribute/meta',
		'@type': 'root',
		'rdfs:label': { '@value': 'Meta-Attributes' }
	},
	{
		'@id': 'root/attribute/user',
		'@type': 'root',
		'rdfs:label': { '@value': 'User-Attributes' },
		'p:attributeset': { '@id': 'attribute/user' }
	},
	{
		'@id': 'root/tag',
		'@type': 'root',
		'rdfs:label': { '@value': 'Tags' },
		'p:attributeset': { '@id': 'attribute/tag' }
	},
	{
		'@id': 'root/link',
		'@type': 'root',
		'rdfs:label': { '@value': 'Link-Types' },
		'p:attributeset': { '@id': 'attribute/link' }
	},
];
const SystemAttributes = [
	{
		'@id': 'attribute/label',
		'@type': 'attribute/user',
		'rdfs:label': { '@value': 'Title' },
		'p:input': { '@id': 'attribute/input/textfield' },
		'p:valueType': { '@id': 'xsd:string' },
		'p:attributeTarget': { '@id': 'rdfs:label' },
		'p:required': { '@value': true },
		'p:system': { '@value': true }
	},
	{
		'@id': 'attribute/created',
		'@type': 'attribute/root',
		'rdfs:label': { '@value': 'Created' },
		'p:input': { '@id': 'attribute/input/datetime' },
		'p:valueType': { '@id': 'xsd:datetime' },
		'p:attributeTarget': { '@id': 'p:created' },
		'p:required': { '@value': false },
		'p:sortable': { '@value': true },
		'p:system': { '@value': true }
	},
	{
		'@id': 'attribute/updated',
		'@type': 'attribute/root',
		'rdfs:label': { '@value': 'Updated' },
		'p:input': { '@id': 'attribute/input/datetime' },
		'p:valueType': { '@id': 'xsd:datetime' },
		'p:attributeTarget': { '@id': 'p:updated' },
		'p:required': { '@value': false },
		'p:sortable': { '@value': true },
		'p:system': { '@value': true }
	},
	{
		'@id': 'prop/link',
		'@type': 'root/link',
		'rdfs:label': { '@value': 'Generic Link' },
		'p:system': { '@value': true }
	}
];
const UserAttributes = [
	{
		'@id': 'attribute/noteContent',
		'@type': 'root/attribute/user',
		'rdfs:label': { '@value': 'Content' },
		'p:input': { '@id': 'attribute/input/textarea' },
		'p:valueType': { '@id': 'xsd:string' },
		'p:attributeTarget': { '@id': 'prop/noteContent' },
		'p:required': { '@value': true }
	},
];
const UserTypes = [
	{
		'@id': 'type/module',
		'@type': 'root/type',
		'rdfs:label': { '@value': 'Module' },
		'p:hasAttribute': [
		]
	},
	{
		'@id': 'type/note',
		'@type': 'root/type',
		'rdfs:label': { '@value': 'Note' },
		'p:hasAttribute': [
			{ '@id': 'attribute/noteContent' }
		]
	},
];
const UserLinks = [
	{
		'@id': 'link/data',
		'@type': 'root/link',
		'rdfs:label': { '@value': 'Data-Flow' }
	}
];
const UserTags = [
	{
		'@id': 'tag/misc',
		'@type': 'root/tag',
		'rdfs:label': { '@value': 'Misc' },
		'p:selectable': { '@value': false }
	},
	{
		'@id': 'tag/important',
		'@type': 'root/tag',
		'p:parent': { '@id': 'tag/misc' },
		'rdfs:label': { '@value': 'Important' },
		'p:selectable': { '@value': true },
		'p:color': { '@value': '#aa0000' }
	},
	{
		'@id': 'tag/done',
		'@type': 'root/tag',
		'p:parent': { '@id': 'tag/misc' },
		'rdfs:label': { '@value': 'Done' },
		'p:selectable': { '@value': true },
		'p:color': { '@value': '#008800' }
	},
]
const UserContent = [
	{
		'@id': 'module/server',
		'@type': 'type/module',
		'p:link': { '@id': 'module/frontend' },
		'rdfs:label': { '@value': 'local server' },
		'p:created': {
			'@value': '2022-12-24T12:14:00',
			'@type': 'xsd:dateTime'
		},
		'p:tag': [
			{ '@id': 'tag/important' }
		]
	},
	{
		'@id': 'module/frontend',
		'@type': 'type/module',
		'rdfs:label': { '@value': 'browser frontend' },
		'p:created': {
			'@value': '2022-12-24T15:14:00',
			'@type': 'xsd:dateTime'
		}
	},
	{
		'@id': 'note/1',
		'@type': 'type/note',
		'p:link': { '@id': 'module/server' },
		'rdfs:label': { 
			'@value': 'this server is important', 
			'@type': 'xsd:string' 
		},
		'p:created': {
			'@value': '2022-12-24T11:00:01',
			'@type': 'xsd:dateTime'
		},
		'p:tags': [
			{ '@id': 'tag/important' }
		]
	},
	{
		'@id': 'note/2',
		'@type': 'type/note',
		'p:link': { '@id': 'module/server' },
		'rdfs:label': { 
			'@value': 'there needs to be http and rmq endpoints', 
			'@type': 'xsd:string' 
		},
		'p:created': {
			'@value': '2022-12-24T12:00:01',
			'@type': 'xsd:dateTime'
		}
	},
	{
		'@id': 'note/3',
		'@type': 'type/note',
		'p:link': { '@id': 'module/server' },
		'rdfs:label': { 
			'@value': 'security here, endpoint for p2p', 
			'@type': 'xsd:string' 
		},
		'p:noteContent': {
			'@value': 'this is a long note about security. this is a long note about security. this is a long note about security. this is a long note about security. this is a long note about security. '
		},
		'p:created': {
			'@value': '2022-12-24T11:00:01',
			'@type': 'xsd:dateTime'
		},
		'p:tags': [
			{ '@id': 'tag/important' }
		]
	}
]

function getDefault() {
	return {
		...mixin,
		'@graph': [
			...Roots,
			...SystemAttributes,
			...UserAttributes,
			...UserTypes,
			...UserTags,
			...UserLinks,
			...UserContent
		]
	}
}



const LocalStoragePrefix = 'lighthouse_save_slot';
const LocalStorageIndex = 'lighthouse_save_index';

type MetaSaveInfo = {
	currentSlot: number,
	slotInfo: SlotInfo[]
}
type SlotInfo = {
	slot: number,
	label: string,
	lastSaved: string
}

const meta: MetaSaveInfo = {
	currentSlot: -1,
	slotInfo: []
};

export async function init() {
	let root = await Graph.createNormalized(getDefault());
	console.log('reset doc', root);
	let store = useMainStore();
	store.openDoc(root);
	loadMeta();
	meta.currentSlot = -1;
}

function loadMeta() {
	let str = localStorage.getItem(LocalStorageIndex);
	if (str) {
		let tmp = JSON.parse(str)
		Object.entries(tmp).forEach(([k, v]) => {
			meta[k] = v;
		});
	}
	console.log('load meta', meta);
}
function saveMeta() {
	console.log('save meta', meta)
	localStorage.setItem(LocalStorageIndex, JSON.stringify(meta));
}

export async function save(newSlot: boolean, label?: string) {
	const slot = newSlot || meta.currentSlot < 0 ? Date.now() : meta.currentSlot;
	const store = useMainStore();
	const key = `${LocalStoragePrefix}_${slot}`;
	localStorage.setItem(key, JSON.stringify(store.root));
	//update meta
	meta.currentSlot = slot;
	let info = meta.slotInfo.find(e => e.slot === slot);
	if (!info) {
		if (!label) label = '?'
		info = {
			slot,
			label,
			lastSaved: ''
		}
		meta.slotInfo.push(info)
	}
	info.lastSaved = new Date().toJSON()
	saveMeta();
}

export async function load(slot: number): Promise<boolean> {
	const key = `${LocalStoragePrefix}_${slot}`;
	let file = localStorage.getItem(key);
	if (file === null) return false;
	let data = JSON.parse(file);
	let root = await Graph.createNormalized({
		...mixin,
		...data
	});
	console.log('open doc', root);
	let store = useMainStore();
	store.openDoc(root);
	meta.currentSlot = slot;
	saveMeta();
	return true;
}

export function remove(slot: number) {
	let idx = meta.slotInfo.findIndex(e => e.slot === slot);
	if (idx < 0) return false;
	meta.slotInfo.splice(idx, 1);
	const key = `${LocalStoragePrefix}_${slot}`;
	localStorage.removeItem(key);
	saveMeta();
	return true;
}

export function getMetaInfo(): MetaSaveInfo {
	return meta;
}
