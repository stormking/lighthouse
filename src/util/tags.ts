import graphConfig from '@/assets/graph.json'
import { useMainStore } from '@/stores/document';
import { Node } from 'starqld';



export function tagsToTree() {
	const store = useMainStore();
	const tags = store.byType(graphConfig.tagRoot);
	const roots = tags.filter(e => !e.getOne(graphConfig.parent));;
	const tagToTree = tag => ({
		value: tag.getId(),
		label: tag.getLiteral(graphConfig.labelProp) || '?',
		disabled: !tag.getLiteral(graphConfig.selectable)
	});
	const add = (parent) => {
		let children = getChildrenFor(parent.value)
		parent.children = children.map(tagToTree);
		parent.children.forEach(add)
	};
	const getChildrenFor = (id: string) => {
		return tags.filter(e => {
			let p = e.getOne(graphConfig.parent);
			if (p instanceof Node) {
				return p.getId() === id;
			}
			return false;
		})
	}
	const rootNodes = roots.map(tagToTree);
	rootNodes.forEach(add);
	return rootNodes;
}
