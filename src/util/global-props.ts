import GraphConfig from '@/assets/graph.json';
import type { Node } from 'starqld';

export default function setup(app) {
	app.config.globalProperties.$filters = {
		label(node: Node): string {
			return node.getLiteral(GraphConfig.labelProp) || '?? ('+node.getId()+')'
		},
		dateTime(value: string): string {
			if (!value) return '';
			let d = new Date(value);
			return d.toLocaleDateString()+' '+d.toLocaleTimeString()
		}
	}
	app.config.globalProperties.$graphConfig = GraphConfig
}
