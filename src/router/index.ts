import { createRouter, createWebHistory, createWebHashHistory } from 'vue-router'
import HomeView from '../views/home.vue'
import NodeListView from '../views/node-list.vue'
import EntityListView from '../views/entity-list.vue'
import NodeView from '../views/node.vue'
import EntityView from '../views/entity.vue'
import LoadView from '../views/load.vue'
import LensView from '../views/lens.vue'

const router = createRouter({
	history: createWebHashHistory(import.meta.env.BASE_URL),
	routes: [
		{
			path: '/',
			name: 'home',
			component: HomeView
		},
		{
			path: '/nodelist',
			name: 'list',
			component: NodeListView
		},
		{
			path: '/entitylist',
			name: 'entity-list',
			component: EntityListView
		},
		{
			path: '/node/:id',
			name: 'node',
			component: NodeView
		},
		{
			path: '/entity',
			name: 'entity',
			component: EntityView
		},
		{
			path: '/load',
			name: 'load',
			component: LoadView
		},
		{
			path: '/lens',
			name: 'lens',
			component: LensView
		}
	]
})

export default router
