import { createApp } from 'vue'
import { createPinia } from 'pinia'
import { init } from './util/file';
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import App from './App.vue'
import router from './router'
import setupGlobalProps from './util/global-props'
import * as ElementPlusIconsVue from '@element-plus/icons-vue'

import './assets/main.less'

const app = createApp(App)

app.use(createPinia())
app.use(router)
app.use(ElementPlus)

await init();

for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
	app.component(key, component)
}

setupGlobalProps(app);

app.mount('#app')
