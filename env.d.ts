/// <reference types="vite/client" />

import GraphConfig from '@/assets/graph.json';
import type { Node } from 'starqld';

export {}

declare module 'vue' {
	interface ComponentCustomProperties {
		$filters: {
			label(node: Node): string,
			dateTime(value: string): string
		},
		$graphConfig: typeof GraphConfig
	}
}

